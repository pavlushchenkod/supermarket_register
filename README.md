# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Order processing for supermarket register with discounts and presents.

### How do I get set up? ###

1. create new virtualenv: 
	virtualenv venv -p python3.6
2. install libraries: 
	pip install -r requirements.txt
3. creating and configuring a database: 
	CREATE USER dbuser WITH PASSWORD '123';
	CREATE DATABASE order_db;
	GRANT ALL PRIVILEGES ON DATABASE order_db to dbuser;
4. starting migrations: 
	python manage.py migrate