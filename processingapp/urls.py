from django.conf.urls import url
from . import views

urlpatterns = [

    url(r'^add$', views.add_goods, name='add_goods'),
    url(r'^del/(?P<name_item_for_delete>[0-9a-zA-Z]+)/$', views.del_goods, name='del_goods'),
    url(r'^next/$', views.next_order, name='next_order'),

]