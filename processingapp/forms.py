from django import forms


class AddGoods(forms.Form):
    name = forms.CharField(label='name', max_length=20)
    quantity = forms.FloatField(label='quantity')