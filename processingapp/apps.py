from django.apps import AppConfig


class ProcessingappConfig(AppConfig):
    name = 'processingapp'
