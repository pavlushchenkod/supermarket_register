class Total:
    def __init__(self, quantity, price, promotion_condition=0, discount_price=0):
        self.quantity = quantity
        self.price = price
        self.promotion_condition = promotion_condition
        self.discount_price = discount_price

    def calculator_for_items(self):
        amount_for_item = self.quantity*self.price
        return amount_for_item

    def calculator_for_weight(self):
        amount_for_item = self.quantity * self.price
        return amount_for_item


class Promotion(Total):
    def calculator_with_discount(self):
        amount_for_item = ((self.quantity // self.promotion_condition) * self.discount_price +
                                    (self.quantity % self.promotion_condition)*self.price)
        return amount_for_item

    def calculator_with_present(self):
        amount_for_item = self.calculator_for_items()
        quantity_presents = self.quantity // self.promotion_condition
        return amount_for_item, quantity_presents

