from processingapp.modules.calculators import Total, Promotion


def calculations(item, goods_in_order):
    quantity_for_item = goods_in_order[item.name]

    if not item.promotion:
        if item.unit_of_measurement == 'Item':
            item = Total(quantity_for_item, item.price)
            amount_for_item = item.calculator_for_items()
            return amount_for_item

        elif item.unit_of_measurement == 'Weight':
            item = Total(quantity_for_item, item.price)
            amount_for_item = item.calculator_for_weight()
            return amount_for_item

    else:
        if item.promotion.type_of_promotion == 'DISCOUNT':
            item = Promotion(quantity_for_item,
                             item.price,
                             item.promotion.promotion_condition,
                             item.promotion.discount_price)
            amount_for_item = item.calculator_with_discount()
            return amount_for_item

        elif item.promotion.type_of_promotion == 'PRESENT':
            name_of_present = item.promotion.promotional_present
            item = Promotion(quantity_for_item,
                             item.price,
                             item.promotion.promotion_condition)
            amount_for_item, quantity_presents = item.calculator_with_present()

            if quantity_presents == 0:
                return amount_for_item
            else:
                presents = {name_of_present: quantity_presents}
                return amount_for_item, presents




