from django.shortcuts import render, redirect
from django.utils.datastructures import MultiValueDictKeyError

from processingapp.modules.functions import calculations
from processingapp.models import Goods, Orders
from .forms import AddGoods


def home_page(request):
    if request.method == 'GET':
        all_available_goods = Goods.objects.all()
        information_messages = []
        try:
            information_messages += [request.GET['message']]
        except MultiValueDictKeyError:
            pass

        if 'goods' in request.session:
            goods_in_order = request.session['goods']

            list_of_goods_objects = []
            list_of_presents = {}
            total_for_order = 0
            for name in goods_in_order.keys():
                object_item = Goods.objects.get(name=name)

                result_calculations = calculations(object_item, goods_in_order)
                if type(result_calculations) != tuple:
                    amount_for_item = result_calculations
                else:
                    amount_for_item, presents = result_calculations
                    list_of_presents.update(presents)
                    request.session['presents'] = list_of_presents

                list_of_goods_objects.append(object_item)
                total_for_order += amount_for_item
                request.session['total_for_order'] = total_for_order

            context = {'all_available_goods': all_available_goods,
                       'goods_in_order': goods_in_order,
                       'list_of_goods_objects': list_of_goods_objects,
                       'total_for_order': total_for_order,
                       'list_of_presents': list_of_presents,
                       'information_messages': information_messages}

        else:
            context = {'all_available_goods': all_available_goods,
                       'information_messages': information_messages}

        return render(request, 'processingapp/main.html', context)


def add_goods(request):
    if request.method == 'POST':
        form = AddGoods(request.POST)
        if form.is_valid():
            name_new_item = form.cleaned_data['name']
            quantity_new_item = form.cleaned_data['quantity']

            if 'goods' in request.session:
                goods = request.session['goods']

                if name_new_item in goods:
                    goods[name_new_item] += quantity_new_item
                else:
                    goods[name_new_item] = quantity_new_item
                request.session['goods'] = goods

            else:
                request.session['goods'] = {name_new_item: quantity_new_item}

            return redirect('/?message=Item successfully added!')

        return redirect('/?message=Enter the correct information!')


def del_goods(request, name_item_for_delete):
    if request.method == 'GET':
        goods = request.session['goods']
        del goods[name_item_for_delete]
        request.session['goods'] = goods
        return redirect('/?message=Item deleted successfully!')


def next_order(request):
    if request.method == 'GET':
        if request.session['presents']:
            presents = request.session['presents']
        else:
            presents = None

        information_about_order = Orders.objects.create(goods=request.session['goods'],
                                                        presents=presents,
                                                        total=request.session['total_for_order'])
        information_about_order.save()
        del request.session['goods']
        return redirect('/?message=New order!')
