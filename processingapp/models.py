from django.db import models
from django.contrib.postgres.fields import JSONField

TYPES_OF_PROMOTION = (
        ('DISCOUNT', 'Discount'),
        ('PRESENT', 'Present'))

TYPES_OF_UNIT_OF_MEASUREMENT = (
        ('Item', 'Item'),
        ('Weight', 'Weight'))


class Promotion(models.Model):
    type_of_promotion = models.CharField(max_length=30,
                                         choices=TYPES_OF_PROMOTION)
    promotion_condition = models.IntegerField(null=True, default=None)
    discount_price = models.IntegerField(null=True, default=None)
    promotional_present = models.CharField(max_length=10, null=True, default=None)

    def __str__(self):
        return self.type_of_promotion


class Goods(models.Model):
    name = models.CharField(max_length=10, unique=True)
    description = models.TextField(max_length=40, null=True)
    price = models.IntegerField()
    unit_of_measurement = models.CharField(max_length=10,
                                           choices=TYPES_OF_UNIT_OF_MEASUREMENT,
                                           default='Item')
    promotion = models.ForeignKey(Promotion, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Orders(models.Model):
    add_datetime = models.DateTimeField(auto_now_add=True)
    goods = JSONField()
    presents = JSONField(null=True)
    total = models.IntegerField()

    def __str__(self):
        return self.goods




